#!/bin/bash
set -e

does_image_exist() {
  [[ "$(docker images -q "$1" 2>/dev/null)" != "" ]]
}

TAG=rpi4-arm64-build

if ! does_image_exist "$TAG":1 ; then
  docker build -t "$TAG":1 -f stage1.dockerfile .
fi

if ! does_image_exist "$TAG":2 ; then
  if docker run -it --name temp-"$TAG" --privileged "$TAG":1 debootstrap --arch=arm64 --include kmod buster /root/rootfs ; then
    docker commit temp-"$TAG" "$TAG":2
    docker rm temp-"$TAG"
  else
    docker rm temp-"$TAG"
    exit 1
  fi
fi

if ! does_image_exist "$TAG":3 ; then
  if \
    docker run -it --name temp-"$TAG" --privileged "$TAG":2 \
      chroot /root/rootfs \
        bash -c 'echo -e "linuxpassword\nlinuxpassword" | passwd root'
  then
    docker commit temp-"$TAG" "$TAG":3
    docker rm temp-"$TAG"
  else
    docker rm temp-"$TAG"
    exit 1
  fi
fi


if ! does_image_exist "$TAG":4; then
  docker build -t "$TAG":4 -f stage2.dockerfile --build-arg from="$TAG":3 .
fi

docker create --name temp-"$TAG" "$TAG":4
docker cp temp-"$TAG":/root/bootfs.tar.xz . && docker cp temp-"$TAG":/root/rootfs.tar.xz . || true
docker rm temp-"$TAG"
