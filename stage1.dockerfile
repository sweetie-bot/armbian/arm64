FROM ubuntu

RUN apt-get update -qq \
 && apt-get install -qqy --no-install-recommends \
        build-essential git ca-certificates \
        bc bison flex libssl-dev \
        debootstrap qemu-user-static \
 && mkdir -p ~/rootfs/usr/bin && ln /usr/bin/qemu-aarch64-static ~/rootfs/usr/bin/

RUN git clone -q --depth=1 https://github.com/raspberrypi/tools ~/tools \
 && git clone -q --depth=1 https://github.com/raspberrypi/linux ~/linux
