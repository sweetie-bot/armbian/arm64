ARG from
FROM $from

ARG KERNEL=kernel8-p4
CMD ["/bin/bash"]

# && export PATH=$PATH:~/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin \

RUN apt-get install -qqy gcc-aarch64-linux-gnu g++-aarch64-linux-gnu \
 && cd ~/linux \
 && make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- bcm2711_defconfig \
 && echo Compiling... \
 && make -j"$(nproc)" ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- zImage modules dtbs

RUN cd ~/linux \
 && PATH=$PATH:~/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin \
    make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=/root/rootfs modules_install \
 && mkdir -p ~/bootfs/overlays \
 && cp arch/arm/boot/zImage ~/bootfs/$KERNEL.img \
 && cp arch/arm/boot/dts/*.dtb ~/bootfs/ \
 && cp arch/arm/boot/dts/overlays/*.dtb* ~/bootfs/overlays/ \
 && cp arch/arm/boot/dts/overlays/README ~/bootfs/overlays/ \
 && cd ~/bootfs && tar cvfJ ~/bootfs.tar.xz . \
 && cd ~/rootfs && tar cvfJ ~/rootfs.tar.xz . \
 && rm -rf ~/bootfs ~/rootfs
